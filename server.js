const express = require("express");
const axios = require("axios");
const next = require("next");
const redis = require("redis");

const isDev = process.env.NODE_ENV !== "production";
const app = next({ isDev });
const handle = app.getRequestHandler();

const PORT = process.env.PORT || 3000;
const REDIS_PORT = process.env.REDIS_PORT || 6379;

app
  .prepare()
  .then(() => {
    const redisClient = redis.createClient(REDIS_PORT);
    const server = express();

    // middleware to catch the fetch request and check/return the chached value
    const cacheMiddleware = (req, res, next) => {
      redisClient.get(
        "categories-5d9d9381ed65340064e4e142-1570608001873-98394100",
        (err, data) => {
          if (err) throw err;

          if (data !== null) {
            console.log("from cache");
            return res.json(JSON.parse(data));
          } else next();
        }
      );
    };

    // method to fetch categories
    const getCategories = async (req, res, next) => {
      console.log("fetching categories...");
      const { username } = req.params;

      const response = await axios.get(
        "https://api.altoshift.com/v1/categories?token=5d9d9381ed65340064e4e142-1570608001873-98394100"
      );

      redisClient.setex(
        "categories-5d9d9381ed65340064e4e142-1570608001873-98394100",
        3600,
        JSON.stringify(response.data)
      );

      return res.json(response.data);
    };

    // define URL for home
    server.get("/home", (req, res) => {
      const page = "/index";
      app.render(req, res, page);
    });

    // define URL and its resolver
    server.get("/categories", cacheMiddleware, getCategories);

    // start the server
    server.listen(PORT, err => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${PORT}`);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
